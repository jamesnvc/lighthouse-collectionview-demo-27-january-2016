//
//  ViewController.h
//  CollectingViews
//
//  Created by James Cash on 27-01-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>


@end

