//
//  CircleLayout.m
//  CollectingViews
//
//  Created by James Cash on 27-01-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "CircleLayout.h"

@interface CircleLayout ()
@property (strong,nonatomic) NSArray* points;
@end

@implementation CircleLayout

- (void)prepareLayout
{
    NSInteger nCircles = [self.collectionView numberOfSections];
    CGFloat r = 100;
    CGFloat Δr = 100;
    CGPoint center = self.collectionView.center;
    NSMutableArray* allPoints = [[NSMutableArray alloc] init];
    for (int section = 0; section < nCircles; section++) {
        NSMutableArray* sectionPoints = [[NSMutableArray alloc] init];
        NSInteger nItems = [self.collectionView numberOfItemsInSection:section];
        CGFloat θ = (2 * M_PI) / nItems;
        CGFloat sectionR = r + section * Δr;
        for (int item = 0; item < nItems; item++) {
            CGFloat x = center.x + sectionR * cos(θ * item);
            CGFloat y = center.y + sectionR * sin(θ * item);
            CGPoint itemPoint = CGPointMake(x, y);
            [sectionPoints addObject:[NSValue valueWithCGPoint:itemPoint]];
        }
        [allPoints addObject:sectionPoints];
    }
    self.points = allPoints;
}

-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes* attrs = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];

    NSArray *sectionPoints = self.points[indexPath.section];
    NSValue *pointValue = sectionPoints[indexPath.item];
    CGPoint point = [pointValue CGPointValue];
    attrs.center = point;

    attrs.size = CGSizeMake(50, 50);
    return attrs;
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray* attributes = [[NSMutableArray alloc] init];
    for (int section = 0; section < [self.collectionView numberOfSections]; section++) {
        for (int item = 0; item < [self.collectionView numberOfItemsInSection:section]; item++) {
            [attributes addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:item inSection:section]]];
        }
    }
    return attributes;
}

@end
