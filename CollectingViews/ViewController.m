//
//  ViewController.m
//  CollectingViews
//
//  Created by James Cash on 27-01-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "CircleLayout.h"

@interface ViewController ()
@property (strong,nonatomic) UICollectionViewFlowLayout* bigLayout;
@property (strong,nonatomic) UICollectionViewFlowLayout* smallLayout;
@property (strong,nonatomic) CircleLayout* crazyLayout;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.bigLayout = [[UICollectionViewFlowLayout alloc] init];
//    self.bigLayout.itemSize = CGSizeMake(200, 150);
    self.bigLayout.minimumInteritemSpacing = 75;
    self.bigLayout.minimumLineSpacing = 100;
    self.bigLayout.sectionInset = UIEdgeInsetsMake(50, 20, 50, 20);
    self.bigLayout.headerReferenceSize = CGSizeMake(CGRectGetWidth(self.collectionView.frame), 100);
    self.bigLayout.footerReferenceSize = CGSizeMake(CGRectGetWidth(self.collectionView.frame), 50);

    self.smallLayout = [[UICollectionViewFlowLayout alloc] init];
//    self.smallLayout.itemSize = CGSizeMake(100, 50);
    self.smallLayout.minimumInteritemSpacing = 10;
    self.smallLayout.minimumLineSpacing = 25;
    self.smallLayout.headerReferenceSize = CGSizeMake(150, 50);
    self.smallLayout.footerReferenceSize = CGSizeMake(150, 25);

    self.crazyLayout = [[CircleLayout alloc] init];

    self.collectionView.collectionViewLayout = self.bigLayout;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleLayout:(id)sender {
    UICollectionViewLayout *old = self.collectionView.collectionViewLayout;
    UICollectionViewLayout *new = nil;
    if (old == self.bigLayout) {
        new = self.smallLayout;
    } else if (old == self.smallLayout) {
        new = self.crazyLayout;
    } else {
        new = self.bigLayout;
    }
    [old invalidateLayout];
    [self.collectionView setCollectionViewLayout:new animated:YES];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5 + section * 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    UILabel *cellLabel = (UILabel*)[cell viewWithTag:42];
    cellLabel.text = [NSString stringWithFormat:@"%ld, %ld", (long)indexPath.section, (long)indexPath.item];
    switch (indexPath.section) {
        case 0:
            cell.backgroundColor = [UIColor blueColor];
            break;
        case 1:
            cell.backgroundColor = [UIColor yellowColor];
            break;
        case 2:
            cell.backgroundColor = [UIColor greenColor];
        default:
            break;
    }
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        return [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CollectionHeader" forIndexPath:indexPath];
    } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
        return [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CollectionFooter" forIndexPath:indexPath];
    }
    return nil;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionViewLayout == self.bigLayout) {
        return CGSizeMake(150 + indexPath.item*20, 100 + indexPath.item*20);
    }
    if (collectionViewLayout == self.smallLayout) {
        return CGSizeMake(100 + indexPath.item*5, 50 + indexPath.item*5);
    }

    return CGSizeZero;
}

@end
